# README #

Repository for practice form automation exercise at toolsqa.com.
PracticeForm file contains executors for locators in the PracticeFormLocators file.
Not all locators are defined due to some problems with available identifiers/attributes. The multiselect element was tested for one option value only, seems to work; multiselection option functionality checking may be added later. The last name checking thing seems to ignore the correct textfield and the entire name is put in the first name textfield, even though the identifier chosen seems to be safe (using xpath for the element fails the test).

