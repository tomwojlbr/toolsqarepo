package pages.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PracticeFormLocators {

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Partial Link Test")
    public WebElement firstLinkCheck;

    @FindBy(how = How.LINK_TEXT, using = "Link Test")
    public WebElement secondLinkCheck;

    @FindBy(how = How.NAME, using = "firstname")
    public WebElement firstName;

    @FindBy(how = How.NAME, using = "lastname")
    public WebElement lastName;

    @FindBy(how = How.NAME, using = "sex")
    public WebElement sexTypeThing;

    @FindBy(how = How.NAME, using = "exp")
    public WebElement yearsExp;

    @FindBy(how = How.ID, using = "datepicker")
    public WebElement datePicker;

    @FindBy(how = How.NAME, using = "profession")
    public WebElement selectProfession;

    /*@FindBy(how = How.ID, using = "photo")
    public WebElement photoUpload;*/

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Selenium Automation Hybrid")
    public WebElement seleniumAutomationFmwk;

    /*@FindBy(how = How.NAME, using = "tool")
    public WebElement automationTool;*/

    @FindBy(how = How.ID, using = "continents")
    public WebElement continentSelect;

    @FindBy(how = How.ID, using = "selenium_commands")
    public WebElement seleniumCommand;

    @FindBy(how = How.ID, using = "submit")
    public WebElement submitButton;

    @FindBy(how = How.CLASS_NAME, using = "bcd")
    public WebElement textLines;

    /*@FindBy(how = How.XPATH, using = "/*//*[@id=\"NextedText\"]/text()")
    public WebElement tekst2;*/

}
