package pages.executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import pages.locators.PracticeFormLocators;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.name;

public class PracticeForm {
    private static WebDriver driver;
    private PracticeFormLocators locators;

    public PracticeForm(WebDriver driver) {
        this.driver = driver;
        locators = new PracticeFormLocators();
        PageFactory.initElements(driver, locators);
    }

    public String firstLinkCheck() {
        return locators.firstLinkCheck.getText();
    }

    public String secondLinkCheck() {
        return locators.secondLinkCheck.getText();
    }

    public PracticeForm firstName (String fname) {
        locators.firstName.sendKeys(fname);
        return this;
    }

    public PracticeForm lastName (String lname) {
        locators.firstName.sendKeys(lname);
        return this;
    }

    public PracticeForm sexTypeThing() {
        List<WebElement> sexType = driver.findElements(name("sex"));
        for(WebElement gender : sexType) {
            if (gender.getText().equals("Male")) {
                if (!gender.isSelected()) {
                    gender.click();
                }
                assertTrue(gender.isSelected());
                break;
            }

        }
        return this;
    }

    public PracticeForm yearsExp() {
        List<WebElement> years = driver.findElements(name("exp"));
        for(WebElement yearexp : years) {
            if(!yearexp.isSelected()) {
                yearexp.click();
            }
            assertTrue(yearexp.isSelected());
            break;
        }
        return this;
    }

    public PracticeForm datePicker (String date) {
        locators.datePicker.sendKeys(date);
        return this;
    }
    public PracticeForm selectProfession () {
        List<WebElement> professions = driver.findElements(name("profession"));
        for(WebElement profession : professions) {
            if(!profession.isSelected()) {
                profession.click();
            }
            assertTrue(profession.isSelected());
            break;
        }
        return this;
    }

    /*public PracticeForm photoUpload() {
        locators.photoUpload.click();
        return this;
    }*/

    /*public List<WebElement> automationTool (String tool) {
        return driver.findElements(name(tool));
    }*/

    public PracticeForm continentSelect(String continent) {
        Select selContinent = new Select(locators.continentSelect);
        selContinent.selectByVisibleText(continent);
        return this;
    }

    public PracticeForm seleniumCommand(String command) {
        Select selCommand = new Select(locators.seleniumCommand);
        selCommand.selectByVisibleText(command);
        return this;
    }

    public PracticeForm submitButton() {
        locators.submitButton.getAttribute("Submit");
        return this;
    }

    public String textLines() {
        return locators.textLines.getText();
    }

    /*public String getSecondText() {
        return locators.tekst2.getText();
    }*/
}
