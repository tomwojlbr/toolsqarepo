import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.executors.PracticeForm;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;

public class PracticeFormTest {
    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\ChromeDriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void testPracticeForm() {
        driver.navigate().to("http://toolsqa.com/automation-practice-form/");
        PracticeForm practiceForm = new PracticeForm(driver);

        assertEquals("Partial Link Test", practiceForm.firstLinkCheck());
        assertEquals("Link Test", practiceForm.secondLinkCheck());
        practiceForm.firstName("Pierre");
        practiceForm.lastName("D'Olain");
        practiceForm.sexTypeThing();
        practiceForm.yearsExp();
        practiceForm.datePicker("19 January 2014");
        practiceForm.selectProfession();
        practiceForm.continentSelect("Australia");
        practiceForm.seleniumCommand("Switch Commands");
        practiceForm.submitButton();
        assertEquals("Text1", practiceForm.textLines());
/* assertEquals("Text2", practiceForm.getSecondText()); */

    }
        @AfterClass
        public static void tearDown() {
            driver.close();
            driver.quit();

        }

    }

